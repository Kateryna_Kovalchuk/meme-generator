
export default function Header(){
    return(
        <div className='header'>
            <div className='header-info'>
                <img className='logo'  src='./images/troll-face.png' alt='troll-face-logo' />
                <h2 className='header-title'>Meme Generator</h2>
            </div>
            <div className='header-description'>React Course - Project 3</div>
        </div>
    )
}
