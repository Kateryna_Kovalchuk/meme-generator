import {useState} from 'react';
import memesData from '../memesData';

export default function Meme(){

    const [url, setUrl] = useState('');

    function returnRandomNum(){
        return Math.floor(Math.random() * memesData.data.memes.length);
    }

    function handleClick(){
        setUrl(memesData.data.memes[returnRandomNum()].url);
    }

    return(
        <main className='main'>
            <div className='form'>
                <div className='form-inputs'>
                    <input type='text' placeholder='Top text' className='form-input' />
                    <input type='text' placeholder='Bottom text' className='form-input' />
                </div>
                <button onClick={handleClick} className='form-btn'>Get a new meme image</button>
            </div>
            <img src={url} className='meme-image' alt='meme' />
        </main>
    )
}
